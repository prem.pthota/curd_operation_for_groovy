package com.curd.operation.controller

import com.curd.operation.domain.Users
import com.curd.operation.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/curd")
class UserController {
    @Autowired
    private UserService userService;
    @PostMapping("/save")
    public ResponseEntity<Users> saveUser(@RequestBody Users user) {
        return ResponseEntity.ok(userService.saveUser(user));
    }
    @GetMapping("/get/{id}")
    public ResponseEntity<Users> getUserById(@PathVariable Long id) {
        return ResponseEntity.ok(userService.getUserById(id));
    }
    @GetMapping("/get/all")
    public ResponseEntity<Users> getAllUserDetials() {
        return ResponseEntity.ok(userService.getAllUserDetials());
    }
    @PutMapping("/update/{id}")
    public ResponseEntity<Users> updateUser(@PathVariable Long id,@RequestBody Users user) {
        return ResponseEntity.ok(userService.saveUser(user));
    }
    @DeleteMapping("/delete/{id}")
    public void deleteUserById(@PathVariable Long id) {
        userService.deleteUserById(id);
        ResponseEntity.status(HttpStatus.OK);
    }
}
