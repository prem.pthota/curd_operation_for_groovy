package com.curd.operation.domain

import lombok.Getter
import lombok.Setter

import javax.persistence.*

@Entity
@Table(name = "users")
@Setter
@Getter
class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id
    String name
    String loginId
    String password
    String description
}
