package com.curd.operation.service

import com.curd.operation.domain.Users

interface UserService {
    Users getUserById(Long id);

    List<Users> getAllUserDetials();

    Users saveUser(Users user);

    Users updateUser(Users user);

    void deleteUserById(Long id);
}
