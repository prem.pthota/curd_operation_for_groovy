package com.curd.operation.service.impl

import com.curd.operation.domain.Users
import com.curd.operation.exception.ResourceNotFoundException
import com.curd.operation.repository.UserRepository
import com.curd.operation.service.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository

    @Override
    Users getUserById(Long id) {
        Optional<Users> optionalUsers = userRepository.findById(id)
        if (optionalUsers.isPresent())
            return optionalUsers.get()
        else
            throw new ResourceNotFoundException("Record not found with user id :" + id)
    }

    @Override
    List<Users> getAllUserDetials() {
        return userRepository.findAll()
    }

    @Override
    Users saveUser(Users user) {
        return userRepository.save(user)
    }

    @Override
    Users updateUser(Users user) {
        Optional<Users> optionalUsers = userRepository.findById(user.getId())
        if (optionalUsers.isPresent()) {
            optionalUsers.get().setId(user.getId())
            optionalUsers.get().setName(user.getName())
            optionalUsers.get().setDescription(user.getDescription())
            optionalUsers.get().setPassword(user.getPassword())
            optionalUsers.get().setLoginId(user.getLoginId())
            return userRepository.saveAndFlush(optionalUsers)
        } else
            throw new ResourceNotFoundException("Record not found with user id :" + user.getId())
    }

    @Override
    void deleteUserById(Long id) {
        Optional<Users> optionalUsers = userRepository.findById(id)
        if (optionalUsers.isPresent())
            userRepository.delete(optionalUsers.get())
        else
            throw new ResourceNotFoundException("Record not found with user id :" + id)
    }
}
