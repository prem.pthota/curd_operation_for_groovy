package com.curd.operation.repository

import com.curd.operation.domain.Users
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository extends JpaRepository<Users, Long> {

}