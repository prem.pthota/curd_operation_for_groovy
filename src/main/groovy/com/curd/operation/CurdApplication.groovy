package com.curd.operation

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class CurdApplication {

	static void main(String[] args) {
		SpringApplication.run(CurdApplication, args)
	}

}
